package teammt.timedcommands.cluster.model;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import org.bukkit.Bukkit;

import lombok.Data;
import masecla.mlib.classes.MamlConfiguration;
import masecla.mlib.main.MLib;

@Data
public class CommandCluster {
	protected MLib lib;

	private String name;
	private List<Subcluster> subclusters = new ArrayList<>();
	private ExecutionContext context;
	private ExecutionContext executor;
	private int timer;
	private int delay;

	public CommandCluster(MLib lib, String name) {
		super();
		this.lib = lib;
		this.name = name;

		MamlConfiguration config = lib.getConfigurationAPI().getConfig();

		for (String s : config.getConfigurationSection("command-clusters." + name + ".subclusters").getKeys(false)) {
			String fullPath = "command-clusters." + name + ".subclusters." + s;
			subclusters.add(new Subcluster(this, fullPath));
		}
		
		context = ExecutionContext.valueOf(config.getString("command-clusters." + name + ".context"));
		executor = ExecutionContext.valueOf(config.getString("command-clusters." + name + ".executor"));
		timer = config.getInt("command-clusters." + name + ".timer");
		delay = config.getInt("command-clusters." + name + ".delay");
	}

	public Subcluster getSubclusterByName(String name) {
		for (Subcluster cr : subclusters)
			if (cr.getName().equalsIgnoreCase(name))
				return cr;
		return null;
	}

	private int getTotalSubclusterChance() {
		int result = 0;
		for (Subcluster c : subclusters)
			result += c.getChance();
		return result;
	}

	public Subcluster pickRandomSubcluster() {
		int random = ThreadLocalRandom.current().nextInt(getTotalSubclusterChance());
		int cr = 0;
		for (Subcluster c : subclusters) {
			if (random > cr && random < cr + c.getChance())
				return c;
			cr += c.getChance();
		}
		return null;
	}

	public void executeNextSubCluster() {
		Subcluster subcluster = pickRandomSubcluster();
		if (subcluster != null)
			subcluster.execute();
	}

	private int taskID = 0;

	public void initializeTask() {
		taskID = Bukkit.getScheduler().scheduleSyncRepeatingTask(lib.getPlugin(), this::executeNextSubCluster, delay,
				timer);
	}

	public void closeTask() {
		Bukkit.getScheduler().cancelTask(taskID);
	}
}
