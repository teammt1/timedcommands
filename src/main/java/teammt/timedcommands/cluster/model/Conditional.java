package teammt.timedcommands.cluster.model;

import org.bukkit.entity.Player;

import masecla.mlib.main.MLib;

public class Conditional {
	private String value;
	private String against;
	private Operator operator;
	private MLib lib;

	public Conditional(Subcluster parent, String path) {
		lib = parent.lib;
		if (lib.getConfigurationAPI().getConfig().isSet(path)) {
			this.value = lib.getConfigurationAPI().getConfig().getString(path + ".checked");
			this.against = lib.getConfigurationAPI().getConfig().getString(path + ".against");
			this.operator = Operator.getOperator(lib.getConfigurationAPI().getConfig().getString(path + ".operator"));
			if (operator == null)
				lib.getLoggerAPI().warn("Invalid operator for " + parent.getName() + " -> " + path);
		}
	}

	public boolean evaluate(Player p) {
		if (operator == null)
			return true;

		String againstReplaced = this.against;
		againstReplaced = lib.getPlaceholderAPI().applyOn(againstReplaced, p);

		return operator.evaluate(againstReplaced, value);
	}
}
