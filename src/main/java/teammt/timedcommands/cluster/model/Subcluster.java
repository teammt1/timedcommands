package teammt.timedcommands.cluster.model;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import masecla.mlib.main.MLib;

@Data
public class Subcluster {
	private List<String> commands;
	private int chance;
	private String name;
	private Conditional conditional;
	protected CommandCluster parent;

	@ToString.Exclude
	@Getter(AccessLevel.NONE)
	@Setter(AccessLevel.NONE)
	protected MLib lib;

	public Subcluster(CommandCluster parent, String path) {
		String[] paths = path.split("\\.");
		
		this.name = paths[paths.length - 1];
		this.lib = parent.lib;
		this.chance = lib.getConfigurationAPI().getConfig().getInt(path + ".chance");
		this.commands = lib.getConfigurationAPI().getConfig().getStringList(path + ".commands");
		this.parent = parent;
		this.conditional = new Conditional(this, path + ".conditional");
	}

	public void execute() {
		ExecutionContext context = parent.getContext();
		ExecutionContext executor = parent.getExecutor();
		if (context == null) {
			lib.getLoggerAPI().warn("Invalid context for " + parent.getName() + " -> " + context);
			return;
		}
		if (executor == null) {
			lib.getLoggerAPI().warn("Invalid executor for " + parent.getName() + " -> " + executor);
			return;
		}

		for (String s : commands) {
			if (context.equals(ExecutionContext.CONSOLE)) {
				if (!conditional.evaluate(null))
					return;
				Bukkit.dispatchCommand(Bukkit.getConsoleSender(), parent.lib.getPlaceholderAPI().applyOn(s, null));
			}

			if (context.equals(ExecutionContext.PLAYERS)) {
				for (Player p : Bukkit.getOnlinePlayers()) {
					if (!conditional.evaluate(p))
						continue;
					String txc = lib.getPlaceholderAPI().applyOn(s.replace("%player%", p.getName()), p);

					CommandSender sender = p;
					if (executor.equals(ExecutionContext.CONSOLE))
						sender = Bukkit.getConsoleSender();

					Bukkit.dispatchCommand(sender, txc);
				}
			}
		}
	}
}
