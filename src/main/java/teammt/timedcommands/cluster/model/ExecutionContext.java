package teammt.timedcommands.cluster.model;

public enum ExecutionContext {
    PLAYERS, CONSOLE;
}
