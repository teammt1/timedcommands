package teammt.timedcommands.cluster.model;

import java.util.function.BiFunction;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Operator {
    EQUALS("==", (a, b) -> a.equals(b)),
    NOT_EQUALS("!=", (a, b) -> !a.equals(b)),
    EQUALS_IGNORE_CASE("=~", (a, b) -> a.equalsIgnoreCase(b)),
    NOT_EQUALS_IGNORE_CASE("!~", (a, b) -> !a.equalsIgnoreCase(b)),
    GREATER_THAN_OR_EQUALS(">=", (a, b) -> doubleMap(a, b, (aD, bD) -> aD >= bD)),
    LESS_THAN_OR_EQUALS("<=", (a, b) -> doubleMap(a, b, (aD, bD) -> aD <= bD)),
    GREATER_THAN(">", (a, b) -> doubleMap(a, b, (aD, bD) -> aD > bD)),
    LESS_THAN("<", (a, b) -> doubleMap(a, b, (aD, bD) -> aD < bD));

    private String operator;
    private BiFunction<String, String, Boolean> function;

    public static Operator getOperator(String operator) {
        for (Operator op : values())
            if (op.operator.equals(operator))
                return op;
        return null;
    }

    private static boolean doubleMap(String a, String b, BiFunction<Double, Double, Boolean> function) {
        try {
            double aDouble = Double.parseDouble(a), bDouble = Double.parseDouble(b);
            return function.apply(aDouble, bDouble);
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public boolean evaluate(String a, String b) {
        return function.apply(a, b);
    }
}
