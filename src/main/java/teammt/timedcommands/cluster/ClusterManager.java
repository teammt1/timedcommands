package teammt.timedcommands.cluster;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import masecla.mlib.classes.Registerable;
import masecla.mlib.main.MLib;
import teammt.timedcommands.cluster.model.CommandCluster;

public class ClusterManager extends Registerable {
	private Map<String, CommandCluster> clusters = new HashMap<>();

	public ClusterManager(MLib lib) {
		super(lib);
	}

	@Override
	public void register() {
		super.register();
		this.reload();
	}

	public void reload() {
		this.closeTasks();
		this.clusters.clear();
		for (String clusterName : getClusterNames())
			clusters.put(clusterName, new CommandCluster(lib, clusterName));
		this.initializeTasks();
	}

	public Set<String> getClusterNames() {
		return lib.getConfigurationAPI().getConfig().getConfigurationSection("command-clusters")
				.getKeys(false);
	}

	public CommandCluster getByName(String name) {
		return clusters.get(name);
	}

	public void initializeTasks() {
		clusters.values().forEach(CommandCluster::initializeTask);
	}

	public void closeTasks() {
		clusters.values().forEach(CommandCluster::closeTask);
	}

}
