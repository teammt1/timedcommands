package teammt.timedcommands.commands;

import org.bukkit.command.CommandSender;

import masecla.mlib.annotations.RegisterableInfo;
import masecla.mlib.annotations.SubcommandInfo;
import masecla.mlib.classes.Registerable;
import masecla.mlib.main.MLib;
import teammt.timedcommands.cluster.ClusterManager;
import teammt.timedcommands.cluster.model.CommandCluster;
import teammt.timedcommands.cluster.model.Subcluster;

@RegisterableInfo(command = "timedcommands")
public class TimedCommandsCommand extends Registerable {

	private ClusterManager clusters;

	public TimedCommandsCommand(MLib lib, ClusterManager clusters) {
		super(lib);
		this.clusters = clusters;
	}

	@SubcommandInfo(subcommand = "", permission = "teammt.timedcommands.help")
	public void helpCommand(CommandSender sender) {
		lib.getMessagesAPI().sendMessage("help-message", sender);
	}

	@SubcommandInfo(subcommand = "help", permission = "teammt.timedcommands.help")
	public void helpCommandWArg(CommandSender sender) {
		helpCommand(sender);
	}

	@SubcommandInfo(subcommand = "reload", permission = "teammt.timedcommands.reload")
	public void reloadCommand(CommandSender sender) {
		clusters.closeTasks();
		lib.getConfigurationAPI().reloadAll();
		lib.getMessagesAPI().reloadSharedConfig();
		clusters.reload();
		clusters.initializeTasks();
		lib.getMessagesAPI().sendMessage("reloaded-plugin", sender);
	}

	@SubcommandInfo(subcommand = "execute", permission = "teammt.timedcommands.execute")
	public void handleExecuteCluster(CommandSender sender, String clusterName) {
		CommandCluster cluster = clusters.getByName(clusterName);
		if (cluster == null) {
			lib.getMessagesAPI().sendMessage("invalid-cluster", sender);
			return;
		}

		cluster.executeNextSubCluster();
		lib.getMessagesAPI().sendMessage("executed-cluster", sender);
	}

	@SubcommandInfo(subcommand = "execute", permission = "teammt.timedcommands.execute")
	public void execution(CommandSender sender, String clusterName, String subclusterName) {
		CommandCluster cluster = clusters.getByName(clusterName);
		if (cluster == null) {
			lib.getMessagesAPI().sendMessage("invalid-cluster", sender);
			return;
		}

		Subcluster subcluster = cluster.getSubclusterByName(subclusterName);
		if (subcluster == null) {
			lib.getMessagesAPI().sendMessage("invalid-subcluster", sender);
			return;
		}

		subcluster.execute();
		lib.getMessagesAPI().sendMessage("executed-cluster", sender);
	}
}
