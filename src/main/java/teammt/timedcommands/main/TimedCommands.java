package teammt.timedcommands.main;

import org.bukkit.plugin.java.JavaPlugin;

import masecla.mlib.main.MLib;
import teammt.timedcommands.cluster.ClusterManager;
import teammt.timedcommands.commands.TimedCommandsCommand;

public class TimedCommands extends JavaPlugin {

	private MLib lib;
	private ClusterManager clusterManager;

	@Override
	public void onEnable() {
		this.lib = new MLib(this);
		lib.getConfigurationAPI().requireAll();

		clusterManager = new ClusterManager(lib);
		clusterManager.register();

		new TimedCommandsCommand(lib, clusterManager).register();
	}

	@Override
	public void onDisable() {
		clusterManager.closeTasks();
	}
}
